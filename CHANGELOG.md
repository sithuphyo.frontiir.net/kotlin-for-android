**Note:** This file is manually generated, it goes without saying that I take responsibility for any possible error.
This `CHANGELOG.md` is inspired by [GitLab
CHANGELOG](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CHANGELOG.md)).

## Version 1.0 `versionCode 3` (8-10-2018)

### Added, Fixed, Security, Removed, Deprecated, Changed, Performance, Other (X Merge Requests)

* Add access to all the screens
* Update the GridLayout screen to show all the main developments
* Improve orientation screen to be more evolutive

## Version 1.5 `versionCode 4` (11-10-2018)

### Added, Fixed, Security, Removed, Deprecated, Changed, Performance, Other (X Merge Requests)

* Add a home screen with the table of contents
* Move the previous home screen to about part
* Improve titles in sub screens
* Add link to the code (Alert and architecture)
* Change some labels
* Improve design of Android version list

## Version 1.5 `versionCode 5` (11-10-2018)

### Added, Fixed, Security, Removed, Deprecated, Changed, Performance, Other (X Merge Requests)

* Add colors in a home screen for all sessions
* Add an array of MagicCircles in Material Design screens
* Add descriptions for the mains screens

## Version 1.5 `versionCode 6` (10-11-2018)

### Added, Fixed, Security, Removed, Deprecated, Changed, Performance, Other (X Merge Requests)

* Add screen of the in-app purchase

## Version 1.5 `versionCode 7` (12-12-2018)

### Added

* Add content 1-b
* Add link purchase screen

## Version 1.5 `versionCode 8` (24-01-2019)

### Added
* Add first content part in incremental way
* Add code to access premium

## Version 1.5 `versionCode 9` (21-02-2019)

### Added
* Add quiz

## Version 1.5 `versionCode 10` (10-04-2019)

### Added
* Add content in 2 part


## Version 1.5 `versionCode 11` (29-04-2019)

### Added
* Add content in 2 part

## Version 1.5 `versionCode 12` (6-06-2019)

### Added
* Add an overview of 4 content

## Version 1.5 `versionCode 13/14` (14-08-2019)

### Added
* Add a first version of 4 content with a particular template

### Changed
* Update the general template of part overview

### Performance
* Clean the Manifest file
