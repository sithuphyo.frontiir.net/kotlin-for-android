# Mobile Education Application : Kotlin For Android

[<img src="https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/raw/master/app/src/main/ic_launcher-web.png" width="10%">](https://play.google.com/apps/testing/com.chillcoding.kotlin)

« Kotlin For Android » is a demo of an **Android** application, entirely coded in **Kotlin**!
In particular, it is a show case of **Android** development with **Kotlin** language,
using *Anko* libraries.
It invites you to learn **Kotlin** through different themes:
 * 1o1 Android
 * Kotlin
 * Native User Interface, Material Design
 * Interactive User Interface
 * Adapter, RecyclerView
 * User Settings, Shared Preference
 * Developer Tools
 * Data Base
 * Network
 * Sensor
 * Map 
 * Publication

These themes can be viewed in disorder, like a game who you are the hero.

♦ Don't hesitate to open an issue to send feedback 👉

## Demo

[Download on **Google Play** (Beta version)](https://play.google.com/apps/testing/com.chillcoding.kotlin)

## Article en Français (FR)

 * [Installer un environnement de développement Android \[AK 1\]](https://www.chillcoding.com/blog/2016/08/03/android-studio-installation/)
 * [Comprendre la configuration d'un projet Android Studio \[AK 2\]](https://www.chillcoding.com/blog/2017/09/28/configurer-kotlin-projet-android/)
 * [Dessiner un coeur dans une vue en Kotlin avec Android \[AK 2\]](https://www.chillcoding.com/android-custom-view-heart/)
 * [Android References \[AK 3 D\]](https://www.chillcoding.com/blog/2017/01/27/android-references/)
 * [Top 6 des raccourcis dans Android Studio (AK 3 E)](https://www.chillcoding.com/blog/2016/04/01/android-top-raccourcis/)
 * [Utiliser les extensions Kotlin pour accéder facilement aux éléments de la vue \[AK 4 A\]](https://www.chillcoding.com/blog/2017/10/03/utiliser-extensions-kotlin/)
 * [UI Cheatsheet (AK 4 B)](https://www.chillcoding.com/blog/2017/01/16/android-ui-cheatsheet/)
 * [Utiliser la bibliothèque Anko \[AK 4 C\]](https://www.chillcoding.com/blog/2017/10/09/utiliser-anko-kotlin-android/)
 * [Afficher une liste d'éléments avec un RecyclerView \[AK 6\]](https://www.chillcoding.com/blog/2018/10/22/creer-liste-recyclerview-kotlin-android/)
 * [Utiliser le fichier de Préférences \[AK 7\]](https://www.chillcoding.com/blog/2014/10/10/utiliser-fichier-preferences/)
 * [Créer une base de données locale avec Anko SQLite \[AK 8\]](https://www.chillcoding.com/blog/2018/01/17/creer-bdd-sqlite-kotlin-android/)
 * [Faire une requête HTTP GET avec Retrofit en Kotlin Android (AK 9)](https://www.chillcoding.com/blog/2017/03/14/requete-http-get-retrofit-android/)

 Autres articles :
 * [L'extraordinaire Humanoïde !](https://www.chillcoding.com/blog/2014/08/08/extraordinaire-humanoide/)
 * [Introduction à Kotlin](https://www.chillcoding.com/blog/2017/07/11/android-kotlin-introduction/)
 * [Déployer une app. Android Wear sur une smartwatch](https://www.chillcoding.com/blog/2016/06/14/android-wear-configuration/)
 

## Features

 * Overriding Activity Lifecycle [activity-lifecycle branch](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/1/diffs)
 * Internationalization[1-d-internationalization](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/7/diffs)
 * Using of Anko [2-d-anko](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/28/diffs)
 * Playing with a Grid Layout [4-a-grid-layout branch](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/5/diffs)
 * Use a LinearLayout [4-a-linear-layout](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/3)
 * Use a RelativeLayout [4-a-relative-layout](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/4)
 * Manage phone orientation [4-b-orientation](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/29)
 * Manage internationalization [4-b-color](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/8)
 * Fix NumberPicker [4-c-number-picker](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/39/diffs)
 * Playing with Button [4-c-button](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/9)
 * Override button theme [4-c-button](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/10)
 * Implement a dialog [4-c-alert-dialog](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/25)
 * Navigation Drawer menu [4-d-navigation-drawer branch](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/12)
 * Manage Click [5-a-manage-click](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/26)
 * Manage Navigation Up [5-b-back-navigation](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/40/diffs)
 * Recycler View List [6-item-list-adapter branch](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/14/commits)
 * Shared Preferences Files [7-sahred-preferences branch](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/15/commits)
 * Local Data Base [8-db branch](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/16/diffs)
 * Viewing DB [8-db branch (MR: Improve Display of db on)](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/24/diffs)
 * HTTPS Communication [9-communication branch](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/17/commits)
 * Custom View [10-b-custom-view branch](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/18/diffs)
 * Accelerometer [10-c-accelerometer branch](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/19/diffs)
 * Simple Geolocalisation [11-geo branch](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android/merge_requests/21/diffs)

## Libraries

 * Retrofit - http://square.github.io/retrofit
 * Moshi - https://github.com/square/moshi
 * OkHttp - http://square.github.io/okhttp
