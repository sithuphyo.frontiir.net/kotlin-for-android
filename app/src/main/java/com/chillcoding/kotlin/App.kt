package com.chillcoding.kotlin

import android.app.Application
import android.support.v4.content.res.ResourcesCompat

/**
 * Created by macha on 12/01/2018.
 */
class App : Application() {
    companion object {
        lateinit var instance: App
        val FRAGMENT_SETTINGS = 0
        val FRAGMENT_ABOUT = 1
        val FRAGMENT_WEB_VIEW = 2
        val FRAGMENT_EXP_LIST = 3
        val FRAGMENT_QUIZ = 4
        const val BUNDLE_PART = "BUNDLDE_PART"
        const val BUNDLE_URL = "BUNDLDE_URL"
        const val BUNDLE_TOC = "BUNDLDE_TOC"
        const val BUNDLE_URL_ARRAY = "BUNDLDE_URL_ARRAY"
        val PREF_NAME: String by lazy { instance.getString(R.string.pref_user_name) }
        val PREF_LAST_DOWNLOAD_DATE = "LAST_DOWNLOAD_DATE"
        const val PERMISSIONS_REQUEST_GET_ACCOUNTS: Int = 13
        const val PREF_PAYLOAD = "PREF_PAYLOAD"
        const val PREF_PREMIUM = "PREF_PREMIUM"
        const val PREF_SESSION1 = "PREF_SESSION1"
        val LONG_TIME = 604800000L // 7 * 24 * 60 * 60 * 1000
        val arrayGitUrl: Array<String> by lazy { instance.resources.getStringArray(R.array.gitlab_url) }
        val sColors: List<Int> by lazy {
            listOf(
                    ResourcesCompat.getColor(instance.resources, R.color.colorAndroid, null),
                    ResourcesCompat.getColor(instance.resources, R.color.colorAnimation, null),
                    ResourcesCompat.getColor(instance.resources, R.color.colorTool, null),
                    ResourcesCompat.getColor(instance.resources, R.color.colorNatif, null),
                    ResourcesCompat.getColor(instance.resources, R.color.colorInteractif, null),
                    ResourcesCompat.getColor(instance.resources, R.color.colorAdapter, null),
                    ResourcesCompat.getColor(instance.resources, R.color.colorDb, null),
                    ResourcesCompat.getColor(instance.resources, R.color.colorNetwork, null),
                    ResourcesCompat.getColor(instance.resources, R.color.colorSensor, null),
                    ResourcesCompat.getColor(instance.resources, R.color.colorMap, null),
                    ResourcesCompat.getColor(instance.resources, R.color.colorPublication, null),
                    ResourcesCompat.getColor(instance.resources, R.color.colorKotlin, null)
            )
        }
        val sAndroidImg = intArrayOf(R.drawable.ic_android, R.drawable.ic_android_kotlin, R.drawable.ic_android_natif, R.drawable.ic_android_interactif, R.drawable.ic_android_tool, R.drawable.ic_android_adapter, R.drawable.ic_android_user, R.drawable.ic_android_db)
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}