package com.chillcoding.kotlin

import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.chillcoding.kotlin.adapter.ListAdapterActivity
import com.chillcoding.kotlin.tool.DelegatesExt
import com.chillcoding.kotlin.tool.setFragment
import com.chillcoding.kotlin.view.FragmentContainerActivity
import com.chillcoding.kotlin.view.MapActivity
import com.chillcoding.kotlin.view.fragment.*
import kotlinx.android.synthetic.main.activity_navigation_drawer.*
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.content_navigation_drawer.*
import kotlinx.android.synthetic.main.nav_header_navigation_drawer.*
import org.jetbrains.anko.*
import java.util.*


class NavigationDrawerActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, AnkoLogger {

    private var userName: String by DelegatesExt.preference(this, App.PREF_NAME, "John")

    private val mArrayQuote: Array<String> by lazy { resources.getStringArray(R.array.motivation_quote) }
    private val mRandom = Random()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation_drawer)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            email("macha@chillcoding.com", getString(R.string.email_subject), "¡Ola! What is the code to get Premium, please ?")
        }

        val toggle = object : ActionBarDrawerToggle(this, drawer_layout,
                toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                navTitle.text = userName
            }
        }
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        setFragment(ABCFragment())
        nav_view.setCheckedItem(R.id.nav_abc)
       // seedSharedPreferences()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_abc -> setFragment(ABCFragment())
            R.id.nav_db -> setFragment(DBFragment())
            R.id.nav_info -> startActivity<ListAdapterActivity>()
            R.id.nav_settings -> startActivity<FragmentContainerActivity>(FragmentContainerActivity.FRAGMENT_ID to App.FRAGMENT_SETTINGS)
            R.id.nav_network -> setFragment(NetworkFragment())
            R.id.nav_material -> setFragment(MDFragment())
            R.id.nav_architecture -> setFragment(ArchitectureFragment())
            R.id.nav_about -> startActivity<FragmentContainerActivity>(FragmentContainerActivity.FRAGMENT_ID to App.FRAGMENT_ABOUT)
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun setFragment(fragment: Fragment) {
        setFragment(R.id.main_content, fragment)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.option, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_map -> startActivity<MapActivity>()
            R.id.action_popup -> showAlertOfMotivation()
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onResume() {
        super.onResume()
        info("USER NAME: $userName")
    }

    fun seedSharedPreferences() {
        PreferenceManager.setDefaultValues(this, R.xml.settings, false)
    }

    private fun showAlertOfMotivation() {
        alert(mArrayQuote[mRandom.nextInt(mArrayQuote.size)]) {
            positiveButton(getString(R.string.action_like)) { showAlertOfMotivation() }
            neutralPressed(R.string.label_link) { browse(App.arrayGitUrl[16]) }
            noButton { }
        }.show()
    }

}
