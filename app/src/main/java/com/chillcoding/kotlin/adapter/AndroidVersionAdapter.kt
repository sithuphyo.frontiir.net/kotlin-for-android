package com.chillcoding.kotlin.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.model.MyAndroidVersion
import com.chillcoding.kotlin.tool.inflate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_android_version.*

/**
 * Created by macha on 11/01/2018.`
 */
class AndroidVersionAdapter(val items: Array<MyAndroidVersion>) : RecyclerView.Adapter<AndroidVersionAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindAndroidVersion(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_android_version))
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view), LayoutContainer {
        override val containerView: View?
            get() = itemView

        fun bindAndroidVersion(androidVersion: MyAndroidVersion) {
            with(androidVersion) {
                androidVersionName.text = name
                androidVersionImg.setImageResource(img)
                androidVersionApi.text = " ${view.resources.getString(R.string.text_api)} $api"
                androidVersionYear.text = " ${view.resources.getString(R.string.text_year)} $year"
            }
        }

    }
}