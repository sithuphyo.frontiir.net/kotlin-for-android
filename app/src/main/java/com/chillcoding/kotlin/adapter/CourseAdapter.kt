package com.chillcoding.kotlin.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.model.MobileCourse
import com.chillcoding.kotlin.tool.inflate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_course.view.*

/**
 * Created by macha on 18/01/2018.
 */
class CourseAdapter(val items: List<MobileCourse>) : RecyclerView.Adapter<CourseAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder:ViewHolder, position: Int) {
        holder.bindCourse(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CourseAdapter.ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_course))
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bindCourse(course: MobileCourse) {
            with(itemView) {
                courseTitle.text = course.title
                courseModule.text = course.module
                courseTime.text = "${resources.getString(R.string.text_time)} ${course.time}"
                Picasso.with(context)
                        .load(course.img)
                        .placeholder(R.drawable.android)
                        .error(R.drawable.android)
                        .into(courseImg);

            }
        }
    }
}