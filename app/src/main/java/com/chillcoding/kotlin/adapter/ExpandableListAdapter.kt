package com.chillcoding.kotlin.adapter


import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.tool.inflate
import java.util.*

class ExpandableListAdapter(private val expandableListTitle: List<String>,
                            private val expandableListDetail: HashMap<String, List<String>>) : BaseExpandableListAdapter() {

    override fun getChild(listPosition: Int, expandedListPosition: Int): String {
        return expandableListDetail[expandableListTitle.get(listPosition)]!!.get(expandedListPosition)
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int) = expandedListPosition.toLong()

    override fun getChildView(listPosition: Int, expandedListPosition: Int,
                              isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val expandedListText = getChild(listPosition, expandedListPosition)
        if (convertView == null) {
            convertView = parent.inflate(R.layout.item_list)
        }
        val expandedListTextView = convertView.findViewById<TextView>(R.id.expandedListItem)
        expandedListTextView.text = expandedListText
        return convertView
    }

    override fun getChildrenCount(listPosition: Int) = expandableListDetail.get(expandableListTitle.get(listPosition))!!.size

    override fun getGroup(listPosition: Int): String {
        return expandableListTitle.get(listPosition)
    }

    override fun getGroupCount() = expandableListTitle.size

    override fun getGroupId(listPosition: Int) = listPosition.toLong()

    override fun getGroupView(listPosition: Int, isExpanded: Boolean,
                              convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val listTitle = getGroup(listPosition)
        if (convertView == null) {
            convertView = parent.inflate(R.layout.list_group)
        }
        val listTitleTextView = convertView.findViewById<TextView>(R.id.listTitle)
        listTitleTextView.setTypeface(null, Typeface.BOLD)
        listTitleTextView.text = listTitle
        return convertView
    }

    override fun hasStableIds() = false


    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int) = true
}
