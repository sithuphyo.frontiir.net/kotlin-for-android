package com.chillcoding.kotlin.adapter

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.model.MyAndroidVersion

import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.content_list_adapter.*
import org.jetbrains.anko.browse

class ListAdapterActivity : AppCompatActivity() {

    var mAndroidVersionList = Array<MyAndroidVersion>(20, { MyAndroidVersion("?", 0, 0, 0) })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_adapter)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        seedAndroidVersionList()
        androidVersionRecyclerView.layoutManager = LinearLayoutManager(this)
        androidVersionRecyclerView.adapter = AndroidVersionAdapter(mAndroidVersionList)
    }

    private fun seedAndroidVersionList() {
        var nameArray = resources.getStringArray(R.array.androidVersionName)
        val imgArray = arrayOf(R.drawable.cupcake, R.drawable.donut, R.drawable.eclair, R.drawable.froyo, R.drawable.gingerbread, R.drawable.honeycomb, R.drawable.icecreamsandwich, R.drawable.jellybean, R.drawable.kitkat, R.drawable.lollipop, R.drawable.marshmallow, R.drawable.nougat, R.drawable.oreo)
        val apiArray = arrayOf(3, 4, 7, 8, 10, 13, 15, 18, 20, 21, 23, 24, 26)
        val yearArray = arrayOf(2009, 2009, 2009, 2010, 2010, 2011, 2011, 2012, 2013, 2014, 2015, 2016, 2017)
        for (i in 0..(nameArray.size - 1))
            mAndroidVersionList[i] = MyAndroidVersion(nameArray[i], imgArray[i], apiArray[i], yearArray[i])
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun openGitLabLink(view: View){
        browse(App.arrayGitUrl[6])
    }
}
