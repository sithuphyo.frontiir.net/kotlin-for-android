package com.chillcoding.kotlin.model

import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

/**
 * Created by macha on 15/01/2018.
 */
class CourseDb(private val courseDbHelper: CourseDbHelper = CourseDbHelper.instance) {

    fun requestCourses() = courseDbHelper.use {
        select(MobileCourseTable.NAME, MobileCourseTable.TITLE, MobileCourseTable.MODULE, MobileCourseTable.TIME, MobileCourseTable.IMG).parseList(classParser<MobileCourse>())
    }

    fun saveCourse(course: MobileCourse) = courseDbHelper.use {
        insert(MobileCourseTable.NAME, MobileCourseTable.TITLE to course.title,
                MobileCourseTable.MODULE to course.module,
                MobileCourseTable.TIME to course.time,
                MobileCourseTable.IMG to course.img)
    }

    fun saveCourses(courses: List<MobileCourse>) {
        for (c in courses)
            saveCourse(c)
    }
}