package com.chillcoding.kotlin.model

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.chillcoding.kotlin.App
import org.jetbrains.anko.db.*

/**
 * Created by macha on 15/01/2018.
 */
class CourseDbHelper(ctx: Context = App.instance) : ManagedSQLiteOpenHelper(ctx, DB_NAME, null, DB_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(MobileCourseTable.NAME, true, MobileCourseTable.ID to INTEGER + PRIMARY_KEY,
                MobileCourseTable.TITLE to TEXT,
                MobileCourseTable.MODULE to TEXT,
                MobileCourseTable.TIME to INTEGER,
                MobileCourseTable.IMG to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(MobileCourseTable.NAME, true)
        onCreate(db)
    }

    companion object {
        val DB_NAME = "course.db"
        val DB_VERSION = 2
        val instance by lazy { CourseDbHelper() }
    }
}