package com.chillcoding.kotlin.model

/**
 * Created by macha on 15/01/2018.
 */
data class MobileCourse(val title: String, val module: String, var time: Int, var img: String="lola")
