package com.chillcoding.kotlin.model

/**
 * Created by macha on 09/01/2018.
 */
data class MyAndroidVersion(val name: String, val img: Int, val api: Int, val year: Int)