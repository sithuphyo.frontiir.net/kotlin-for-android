package com.chillcoding.kotlin.model

data class Question(val question: String = "What is Android?", val opt: Array<String> = arrayOf("a mobile", "a banana", "an operating system", "a market"), val ans: Int = 2)