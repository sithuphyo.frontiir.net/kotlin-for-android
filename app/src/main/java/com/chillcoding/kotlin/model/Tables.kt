package com.chillcoding.kotlin.model

/**
 * Created by macha on 15/01/2018.
 */
object MobileCourseTable {
    val NAME = "MobileCourse"
    val ID = "_id"
    val TITLE = "title"
    val MODULE = "module"
    val TIME = "time"
    val IMG = "img"
}