package com.chillcoding.kotlin.network

import com.chillcoding.kotlin.model.MobileCourse
import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by macha on 18/01/2018.
 */
interface CoursesService {
    @GET("/courses")
    fun listCourses(): Call<List<MobileCourse>>
}