package com.chillcoding.kotlin.tool

import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by macha on 12/01/2018.
 */
fun AppCompatActivity.setFragment(@IdRes idRes: Int, fragment: Fragment) {
    supportFragmentManager.beginTransaction()
            .replace(idRes, fragment)
            .commit()
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}