package com.chillcoding.kotlin.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.chillcoding.kotlin.R
import kotlinx.android.synthetic.main.activity_anko.*
import org.jetbrains.anko.email

/**
 * Created by macha on 14/02/2018.
 */
class AnkoActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anko)
        emailImg.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.emailImg -> email("macha@humanchill.io", getString(R.string.email_subject), "...")
        }
    }
}