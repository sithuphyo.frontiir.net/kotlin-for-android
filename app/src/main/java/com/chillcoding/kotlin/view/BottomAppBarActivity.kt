package com.chillcoding.kotlin.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.view.fragment.BottomNavigationViewFragment
import kotlinx.android.synthetic.main.activity_bottom_navigation.*
import org.jetbrains.anko.browse
import org.jetbrains.anko.startActivity


class BottomAppBarActivity : AppCompatActivity() {

    val urlArray: Array<String> by lazy {
        resources.getStringArray(R.array.url_session4)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_navigation)

        setSupportActionBar(bottomAppBar)
        bottomWebView.loadUrl(urlArray[0])

        bottomAppBarFab.setOnClickListener {
            bottomBtnPrez.visibility = View.VISIBLE
            bottomBtnQuiz.visibility = View.VISIBLE
            bottomWebView.loadUrl(urlArray[0])
        }

        val urlPrez = resources.getStringArray(R.array.url_prez)
        bottomBtnPrez.setOnClickListener { browse(urlPrez[3]) }
        bottomBtnQuiz.setOnClickListener { startActivity<FragmentContainerActivity>(FragmentContainerActivity.FRAGMENT_ID to App.FRAGMENT_QUIZ, App.BUNDLE_PART to 3) }

        bottomWebView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
                if (url.contains("file:///android_asset/")) {
                    bottomWebView.loadUrl(url)
                    hideBtn()
                } else
                    browse(url)
                return true
            }

            override fun onLoadResource(view: WebView?, url: String?) {

            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.bottom_option, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navigation_layout -> {
                hideBtn()
                bottomWebView.loadUrl(urlArray[1])
            }
            R.id.navigation_resource -> {
                hideBtn()
                bottomWebView.loadUrl(urlArray[2])
            }
            R.id.navigation_menu -> {
                hideBtn()
                bottomWebView.loadUrl(urlArray[3])
            }
            android.R.id.home -> {
                val bottomNavViewFragment = BottomNavigationViewFragment()
                bottomNavViewFragment.show(supportFragmentManager, bottomNavViewFragment.tag)
            }
            else -> return false

        }
        return true
    }

    fun hideBtn() {
        bottomBtnPrez.visibility = View.GONE
        bottomBtnQuiz.visibility = View.GONE
    }
}
