package com.chillcoding.kotlin.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import kotlinx.android.synthetic.main.activity_button.*
import org.jetbrains.anko.browse

class ButtonActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_button)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        buttonGitLabLink.setOnClickListener { browse(App.arrayGitUrl[5]) }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
