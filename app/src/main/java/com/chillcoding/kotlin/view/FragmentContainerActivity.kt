package com.chillcoding.kotlin.view

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.tool.setFragment
import com.chillcoding.kotlin.view.fragment.*
import kotlinx.android.synthetic.main.app_bar.*

class FragmentContainerActivity : AppCompatActivity() {

    companion object {
        val FRAGMENT_ID = "FragmentContainerActivity:fragmentId"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_container)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        when (intent.getIntExtra(FRAGMENT_ID, 0)) {
            App.FRAGMENT_SETTINGS -> {
                setFragment(SettingsFragment())
                supportActionBar!!.title = getString(R.string.title_settings)
            }
            App.FRAGMENT_ABOUT -> {
                setFragment(AboutFragment())
                supportActionBar!!.title = getString(R.string.title_about)
            }
            App.FRAGMENT_WEB_VIEW -> {
                setFragment(WebViewFragment())
                supportActionBar!!.title = resources.getStringArray(R.array.title_session)[intent.getIntExtra(App.BUNDLE_PART, 0)]
                supportActionBar?.setBackgroundDrawable(ColorDrawable(App.sColors[intent.getIntExtra(App.BUNDLE_PART, 0)]))
            }
            App.FRAGMENT_EXP_LIST -> {
                setFragment(ExpandableListFragment())
                supportActionBar!!.title = resources.getStringArray(R.array.title_session)[intent.getIntExtra(App.BUNDLE_PART, 0)]
                supportActionBar?.setBackgroundDrawable(ColorDrawable(App.sColors[intent.getIntExtra(App.BUNDLE_PART, 0)]))
            }
            App.FRAGMENT_QUIZ -> {
                if (savedInstanceState == null)
                    setFragment(QuizFragment())
                supportActionBar!!.title = getString(R.string.label_quiz)
                supportActionBar?.setBackgroundDrawable(ColorDrawable(App.sColors[intent.getIntExtra(App.BUNDLE_PART, 0)]))
            }
        }
    }

    private fun setFragment(fragment: Fragment) {
        setFragment(R.id.second_content, fragment)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
