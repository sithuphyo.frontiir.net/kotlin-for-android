package com.chillcoding.kotlin.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.view.orientation.OrientationActivity
import kotlinx.android.synthetic.main.activity_grid_layout.*
import org.jetbrains.anko.browse
import org.jetbrains.anko.startActivity

class GridLayoutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_layout)

        gridGitLabLink.setOnClickListener { browse(App.arrayGitUrl[12]) }

        gridLayoutOrientation.setOnClickListener { startActivity<OrientationActivity>() }
        gridLayoutButton.setOnClickListener { startActivity<ButtonActivity>() }
        gridLayoutNumber.setOnClickListener { startActivity<NumberPickerActivity>() }
        gridLayoutLifeCycle.setOnClickListener { startActivity<LifeCycleActivity>() }
        gridLayoutRelative.setOnClickListener { startActivity<RelativeActivity>() }
    }
}
