package com.chillcoding.kotlin.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.browse
import org.jetbrains.anko.info
import org.jetbrains.anko.toast

class LifeCycleActivity : AppCompatActivity(), AnkoLogger {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        lifeGitLabLink.setOnClickListener { browse(App.arrayGitUrl[3]) }

        info(getString(R.string.text_oncreate))
        toast(getString(R.string.text_oncreate))
    }

    override fun onRestart() {
        super.onRestart()
        info(getString(R.string.text_onrestart))
        toast(getString(R.string.text_onrestart))
    }

    override fun onStart() {
        super.onStart()
        info(getString(R.string.text_onstart))
        toast(getString(R.string.text_onstart))
    }

    override fun onResume() {
        super.onResume()
        info(getString(R.string.text_onresume))
        toast(getString(R.string.text_onresume))
    }

    override fun onPause() {
        super.onPause()
        info(getString(R.string.text_onpause))
        toast(getString(R.string.text_onpause))
    }

    override fun onStop() {
        super.onStop()
        info(getString(R.string.text_onstop))
        toast(getString(R.string.text_onpause))
    }

    override fun onDestroy() {
        toast(getString(R.string.text_ondestroy))
        info(getString(R.string.text_ondestroy))
        super.onDestroy()
    }
}
