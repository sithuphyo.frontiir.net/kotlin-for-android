package com.chillcoding.kotlin.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.NumberPicker
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import kotlinx.android.synthetic.main.activity_number_picker.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.browse
import org.jetbrains.anko.info
import org.jetbrains.anko.toast

class NumberPickerActivity : AppCompatActivity(), AnkoLogger {

    var mValues = Array<String>(6, { i: Int -> ((i + 1) * 5).toString() })

    companion object {
        const val BUNDLE_NP_VAL = "key of the np value"

    }

    override fun onCreate(inState: Bundle?) {
        super.onCreate(inState)
        setContentView(R.layout.activity_number_picker)

        numberGitLabLink.setOnClickListener { browse(App.arrayGitUrl[14]) }

        numberPicker.minValue = 0
        numberPicker.maxValue = mValues.size - 1
        numberPicker.displayedValues = mValues
        numberPicker.setOnValueChangedListener { numberPicker: NumberPicker?, oldV: Int, newV: Int ->
            toast("${mValues[numberPicker!!.value]}")
            info(oldV)
        }
        info("$BUNDLE_NP_VAL : ${inState?.getInt(BUNDLE_NP_VAL)}")
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt(BUNDLE_NP_VAL, numberPicker.value)
    }

    override fun onRestoreInstanceState(inState: Bundle?) {
        super.onRestoreInstanceState(inState)

        if (inState != null)
            numberPicker.value = inState.getInt(BUNDLE_NP_VAL)
    }
}
