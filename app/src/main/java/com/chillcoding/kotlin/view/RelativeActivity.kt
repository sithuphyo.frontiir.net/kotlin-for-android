package com.chillcoding.kotlin.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import kotlinx.android.synthetic.main.activity_relative.*
import org.jetbrains.anko.browse

class RelativeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_relative)
        relativeGitLabLink.setOnClickListener{browse(App.arrayGitUrl[15])}
    }
}
