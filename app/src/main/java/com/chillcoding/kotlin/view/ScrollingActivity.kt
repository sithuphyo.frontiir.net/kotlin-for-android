package com.chillcoding.kotlin.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ArrayAdapter
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.tool.DelegatesExt
import kotlinx.android.synthetic.main.activity_scrolling.*
import kotlinx.android.synthetic.main.content_scrolling.*
import org.jetbrains.anko.browse
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class ScrollingActivity : AppCompatActivity() {
    var isPremium: Boolean by DelegatesExt.preference(this, App.PREF_PREMIUM, false)
    var isSession1: Boolean by DelegatesExt.preference(this, App.PREF_SESSION1, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scrolling)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val index = intent.getIntExtra(App.BUNDLE_PART, 0)
        title = resources.getStringArray(R.array.title_session)[index]
        scrollingDetailsImg.setImageResource(App.sAndroidImg[index])
        fab.setOnClickListener { view ->
            startActivity<PurchaseActivity>()
        }
        scrollTextSession.text = resources.getStringArray(R.array.text_session)[index]

        val tocId = resources.obtainTypedArray(R.array.toc_session).getResourceId(index, 0)
        val tocArray = resources.getStringArray(tocId)
        scrollList.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tocArray)

        val urlId = resources.obtainTypedArray(R.array.url_session).getResourceId(index, 0)
        val urlArray = resources.getStringArray(urlId)

        val urlPrez = resources.getStringArray(R.array.url_prez)

        if (isPremium || isSession1) {
            fab.setImageResource(R.drawable.ic_check)
            fab.setOnClickListener { toast("cool") }
            srollBtnPrez.setOnClickListener { browse(urlPrez[index]) }
            scrollBtn.setOnClickListener { startActivity<FragmentContainerActivity>(FragmentContainerActivity.FRAGMENT_ID to App.FRAGMENT_EXP_LIST, App.BUNDLE_TOC to tocArray, App.BUNDLE_URL_ARRAY to urlArray, App.BUNDLE_PART to index) }
            scrollList.setOnItemClickListener { parent, view, position, id ->
                startActivity<FragmentContainerActivity>(FragmentContainerActivity.FRAGMENT_ID to App.FRAGMENT_WEB_VIEW, App.BUNDLE_URL to urlArray[position], App.BUNDLE_PART to index)
            }
            scrollBtnQuiz.setOnClickListener { startActivity<FragmentContainerActivity>(FragmentContainerActivity.FRAGMENT_ID to App.FRAGMENT_QUIZ, App.BUNDLE_PART to index) }
        } else {
            scrollBtn.visibility = View.GONE
            srollBtnPrez.visibility = View.GONE
            scrollBtnQuiz.visibility = View.GONE
            scrollTextRes.visibility = View.GONE
            scrollTextQuiz.visibility = View.GONE
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
