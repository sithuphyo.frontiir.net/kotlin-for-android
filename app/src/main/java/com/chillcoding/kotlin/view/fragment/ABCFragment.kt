package com.chillcoding.kotlin.view.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.tool.DelegatesExt
import com.chillcoding.kotlin.view.BottomAppBarActivity
import com.chillcoding.kotlin.view.PurchaseActivity
import com.chillcoding.kotlin.view.ScrollingActivity
import kotlinx.android.synthetic.main.fragment_abc.*
import org.jetbrains.anko.noButton
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.startActivity

/**
 * A simple [Fragment] subclass.
 */
class ABCFragment : Fragment() {

    var isPremium: Boolean by DelegatesExt.preference(this, App.PREF_PREMIUM, false)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_abc, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sessionTitleArray = resources.getStringArray(R.array.title_session)
        abcSession1.text = sessionTitleArray[0]
        abcSession1.setOnClickListener { startActivity<ScrollingActivity>(App.BUNDLE_PART to 0) }

        abcSession2.text = sessionTitleArray[1]
        abcSession2.setOnClickListener { startActivity<ScrollingActivity>(App.BUNDLE_PART to 1) }

        abcSession3.text = sessionTitleArray[2]
        abcSession4.text = sessionTitleArray[3]
        abcSession4.setOnClickListener {
            if (isPremium)
                startActivity<BottomAppBarActivity>()
            else {
                alert(getString(R.string.text_paying_content)) {
                    positiveButton(getString(R.string.action_buy)) { startActivity<PurchaseActivity>() }
                    noButton { }
                }.show()
            }
        }

        abcSession5.text = sessionTitleArray[4]
        abcSession6.text = sessionTitleArray[5]
        abcSession7.text = sessionTitleArray[6]
        abcSession8.text = sessionTitleArray[7]
        abcSession9.text = sessionTitleArray[8]
        abcSession10.text = sessionTitleArray[9]
        abcSession11.text = sessionTitleArray[10]
        abcSession12.text = sessionTitleArray[11]

    }

}// Required empty public constructor
