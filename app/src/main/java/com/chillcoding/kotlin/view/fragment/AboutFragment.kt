package com.chillcoding.kotlin.view.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.tool.DelegatesExt
import com.chillcoding.kotlin.tool.inflate
import com.chillcoding.kotlin.view.AnkoActivity
import com.chillcoding.kotlin.view.GridLayoutActivity
import kotlinx.android.synthetic.main.fragment_about.*
import org.jetbrains.anko.support.v4.browse
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast

/**
 * Created by macha on 30/01/2018.
 */
class AboutFragment : Fragment(), View.OnClickListener {

    var isPremium: Boolean by DelegatesExt.preference(this, App.PREF_PREMIUM, false)
    private var like: Boolean = true
    private var beach: Boolean = true


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return container!!.inflate(R.layout.fragment_about)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        aboutGitlab.setOnClickListener(this)
        aboutKotlin.setOnClickListener(this)
        aboutMaterial.setOnClickListener(this)
        aboutAndroidStudio.setOnClickListener(this)
        aboutChillcodingButton.setOnClickListener(this)
        aboutAndroid.setOnClickListener(this)
        aboutMachaIcon.setOnClickListener(this)
        aboutMachaTwitterIcon.setOnClickListener(this)
        aboutMachaPlayIcon.setOnClickListener(this)
        aboutMachaGitIcon.setOnClickListener(this)
        aboutILove.setOnClickListener(this)
        aboutMachaInstaIcon.setOnClickListener(this)
        aboutUpdate.setOnClickListener(this)
        aboutChillcodingIcon.setOnClickListener(this)
        aboutChillcodingBeachIcon.setOnClickListener(this)
        aboutChillcodingStarIcon.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.aboutGitlab -> browse("https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android")
            R.id.aboutKotlin -> startActivity<AnkoActivity>()
            R.id.aboutMaterial -> browse("https://www.chillcoding.com/blog/2017/01/16/android-ui-cheatsheet/")
            R.id.aboutAndroidStudio -> browse(getString(R.string.androidStudio_url))
            R.id.aboutChillcodingButton -> browse("https:www.chillcoding.com")
            R.id.aboutAndroid -> startActivity<GridLayoutActivity>()
            R.id.aboutMachaIcon -> browse("https://www.linkedin.com/in/machadacosta/")
            R.id.aboutMachaPlayIcon -> browse("https://play.google.com/store/apps/dev?id=8743577772083036869")
            R.id.aboutMachaGitIcon -> browse("https://gitlab.com/macha")
            R.id.aboutMachaTwitterIcon -> browse("https://twitter.com/machaDaCosta")
            R.id.aboutILove -> browse("https://play.google.com/store/apps/details?id=com.chillcoding.ilove")
            R.id.aboutMachaInstaIcon -> browse("https://www.instagram.com/ilovekotlin/")
            R.id.aboutUpdate -> browse("https://play.google.com/store/apps/details?id=com.chillcoding.kotlin")
            R.id.aboutChillcodingIcon ->
                with(aboutChillcodingIcon) {
                    if (like)
                        setImageResource(R.drawable.ic_love)
                    else
                        setImageResource(R.drawable.ic_love_border)
                    like = !like
                }
            R.id.aboutChillcodingBeachIcon ->
                with(aboutChillcodingBeachIcon) {
                    if (beach)
                        setImageResource(R.drawable.ic_sun)
                    else
                        setImageResource(R.drawable.ic_beach_access)
                    beach = !beach
                }
            R.id.aboutChillcodingStarIcon -> {
                with(aboutChillcodingStarIcon) {
                    if (isPremium)
                        setImageResource(R.drawable.ic_star_border)
                    else
                        setImageResource(R.drawable.ic_star)
                    if (!like && !beach) {
                        isPremium = !isPremium
                        toast("cool")
                    }
                }
            }
        }
    }
}