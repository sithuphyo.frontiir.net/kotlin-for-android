package com.chillcoding.kotlin.view.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.tool.inflate
import kotlinx.android.synthetic.main.fragment_architecture.*
import org.jetbrains.anko.support.v4.browse

/**
 * Created by macha on 29/01/2018.
 */
class ArchitectureFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return container!!.inflate(R.layout.fragment_architecture)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        architectureGitLabLink.setOnClickListener { browse(App.arrayGitUrl[17]) }
    }
}