package com.chillcoding.kotlin.view.fragment

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.view.ButtonActivity
import com.chillcoding.kotlin.view.NumberPickerActivity
import kotlinx.android.synthetic.main.fragment_bottom_nav_view.*
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast

class BottomNavigationViewFragment : BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bottom_nav_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        bottomNavView.setNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.nav_number_picker -> startActivity<NumberPickerActivity>()
                R.id.nav_button -> startActivity<ButtonActivity>()
                else -> toast(R.string.text_come_soon)
            }
            true
        }
    }
}