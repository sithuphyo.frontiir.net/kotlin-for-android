package com.chillcoding.kotlin.view.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.adapter.CourseAdapter
import com.chillcoding.kotlin.model.CourseDb
import com.chillcoding.kotlin.model.MobileCourse
import com.chillcoding.kotlin.tool.inflate
import kotlinx.android.synthetic.main.fragment_db.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.support.v4.browse
import org.jetbrains.anko.uiThread

/**
 * Created by macha on 15/01/2018.
 */
class DBFragment : Fragment(), AnkoLogger {

    val courseDb = CourseDb()
    var list = ArrayList<MobileCourse>()
    var adapter = CourseAdapter(list)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        doAsync {
            val listFromDb = courseDb.requestCourses()
            for (course in listFromDb) {
                list.add(course)
            }
            if (list.size < 1) {
                courseDb.saveCourse(MobileCourse("ABCD Android", "Android", 120))
            }
            uiThread {
                updateUI()
            }
        }
    }

    private fun updateUI() {
        adapter = CourseAdapter(list)
        dbList?.swapAdapter(adapter, true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return container!!.inflate(R.layout.fragment_db)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        dbList.layoutManager = LinearLayoutManager(activity)
        dbList.adapter = adapter
        dbGitLabLink.setOnClickListener { browse(App.arrayGitUrl[8]) }
    }

}