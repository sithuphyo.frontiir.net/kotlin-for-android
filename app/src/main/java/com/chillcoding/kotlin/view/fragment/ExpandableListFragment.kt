package com.chillcoding.kotlin.view.fragment


import android.content.res.TypedArray
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.adapter.ExpandableListAdapter
import com.chillcoding.kotlin.view.FragmentContainerActivity
import kotlinx.android.synthetic.main.fragment_expandable_list.*
import org.jetbrains.anko.support.v4.startActivity

/**
 * A simple [Fragment] subclass.
 *
 */
class ExpandableListFragment : Fragment() {

    lateinit var tocArray: Array<String>

    lateinit var urlArray: Array<String>

    var index = 0

    private var tocSectionArray = HashMap<String, List<String>>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        seedListData()
        return inflater.inflate(R.layout.fragment_expandable_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var listAdapter = ExpandableListAdapter(tocArray.toList(), tocSectionArray)
        detailExpandList.setAdapter(listAdapter)
        detailExpandList.setOnChildClickListener { parent, view, position, positionBis, id ->
            startActivity<FragmentContainerActivity>(FragmentContainerActivity.FRAGMENT_ID to App.FRAGMENT_WEB_VIEW, App.BUNDLE_URL to urlArray[position], App.BUNDLE_PART to index)
            true
        }
    }

    private fun seedListData() {
        activity?.let {
            tocArray = it.intent.getStringArrayExtra(App.BUNDLE_TOC)

            urlArray = it.intent.getStringArrayExtra(App.BUNDLE_URL_ARRAY)

            index = it.intent.getIntExtra(App.BUNDLE_PART, 0)
        }
        val tocSectionId = resources.obtainTypedArray(R.array.toc_section).getResourceId(index, 0)
        val tocSectionIdArray: TypedArray = resources.obtainTypedArray(tocSectionId)

        for (i in 0 until tocSectionIdArray.length()) {
            tocSectionArray.put(tocArray.get(i), resources.getStringArray(tocSectionIdArray.getResourceId(i, 0)).toList())
        }
    }
}
