package com.chillcoding.kotlin.view.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.tool.inflate
import kotlinx.android.synthetic.main.fragment_md.*
import org.jetbrains.anko.support.v4.browse

/**
 * Created by macha on 23/01/2018.
 */
class MDFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return container!!.inflate(R.layout.fragment_md)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mdGitLabLink.setOnClickListener { browse(App.arrayGitUrl[10]) }
    }
}