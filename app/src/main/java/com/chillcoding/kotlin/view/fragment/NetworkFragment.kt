package com.chillcoding.kotlin.view.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.adapter.CourseAdapter
import com.chillcoding.kotlin.model.CourseDb
import com.chillcoding.kotlin.model.MobileCourse
import com.chillcoding.kotlin.network.CoursesService
import com.chillcoding.kotlin.tool.DelegatesExt
import com.chillcoding.kotlin.tool.inflate
import kotlinx.android.synthetic.main.fragment_network.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.support.v4.browse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Created by macha on 18/01/2018.
 */
class NetworkFragment : Fragment(), AnkoLogger {

    var lastDate: Long by DelegatesExt.preference(this as Fragment, App.PREF_LAST_DOWNLOAD_DATE, 0L)
    val current = System.currentTimeMillis()
    private val url = "https://mobile-courses-server.herokuapp.com/"
    var allCourses = ArrayList<MobileCourse>()
    val courseDb = CourseDb()
    var adapter = CourseAdapter(allCourses)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        
        getCoursesFromServer()
    }

    private fun itHasbeenLongTime(): Boolean {
        if (lastDate == 0L)
            return true
        else {
            return ((lastDate - current) > App.LONG_TIME)
        }
    }

    private fun getCoursesFromServer() {

        val retrofit = Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()

        val service = retrofit.create(CoursesService::class.java)

        val courseRequest = service.listCourses()

        courseRequest.enqueue(object : Callback<List<MobileCourse>> {
            override fun onFailure(call: Call<List<MobileCourse>>?, t: Throwable?) {
                info("KO")
            }

            override fun onResponse(call: Call<List<MobileCourse>>, response: Response<List<MobileCourse>>) {
                if (response.body() != null) {
                    info("OK FROM SERVER")
                    val allCoursesFromServer = response.body()!!
                    if (allCoursesFromServer != null) {
                        for (course in allCoursesFromServer) {
                            if (!isInExistingList(course)) {
                                if (itHasbeenLongTime())
                                    courseDb.saveCourse(course)
                                allCourses.add(course)
                            }
                        }
                        lastDate = current
                        updateUI()
                    }
                }
            }
        })
    }

    private fun isInExistingList(course: MobileCourse) = allCourses.any { it.title == course.title }

    private fun updateUI() {
        adapter = CourseAdapter(allCourses)
        networkRecycler?.swapAdapter(adapter, true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return container!!.inflate(R.layout.fragment_network)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        networkRecycler.layoutManager = LinearLayoutManager(activity)
        networkRecycler.adapter = adapter
        networkGitLabLink.setOnClickListener { browse(App.arrayGitUrl[9]) }
    }
}