package com.chillcoding.kotlin.view.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import com.chillcoding.kotlin.model.Question
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import kotlinx.android.synthetic.main.fragment_quiz.*
import okio.Okio
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.toast
import java.io.IOException
import java.nio.charset.Charset


/**
 * A simple quiz [Fragment].
 *
 */
class QuizFragment : Fragment() {

    lateinit var quiz: List<Question>

    var questionIndex = 0
    var question = Question()
    var score = 0

    companion object {
        const val BUNDLE_QUESTION_INDEX = "question index key"
        const val BUNDLE_SCORE = "score key"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_quiz, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getAllQuestions()
        showQuestion()
        quizBtn.setOnClickListener { submitAnswer() }
    }

    fun showNextQuestion() {
        questionIndex++
        quizRadio.clearCheck()
        showQuestion()
    }

    fun showQuestion() {
        if (questionIndex < quiz.size) {
            quizState.text = "${questionIndex+1} / ${quiz.size}"
            question = quiz[questionIndex]
            quizQuestion.text = question.question
            quizOpt0.text = question.opt[0]
            quizOpt1.text = question.opt[1]
            quizOpt2.text = question.opt[2]
            quizOpt3.text = question.opt[3]
        } else
            showScore()
    }

    private fun showScore() {
        quizScore.visibility = View.VISIBLE
        if (score > 0)
            quizScore.text = "${getString(R.string.text_good_match)}\n ${getString(R.string.text_score)} $score / ${quiz.size}"
        else
            quizScore.text = "${getString(R.string.text_replay)}"
        quizQuestion.visibility = View.GONE
        quizRadio.visibility = View.GONE
        quizBtn.visibility = View.GONE
        quizState.visibility = View.GONE
    }

    private fun submitAnswer() {
        var correctAnswer = false
        when (quizRadio.checkedRadioButtonId) {
            R.id.quizOpt0 -> if (question.ans == 0) correctAnswer = true
            R.id.quizOpt1 -> if (question.ans == 1) correctAnswer = true
            R.id.quizOpt2 -> if (question.ans == 2) correctAnswer = true
            R.id.quizOpt3 -> if (question.ans == 3) correctAnswer = true
        }
        if (correctAnswer) {
            toast(R.string.text_good_answer)
            score++
            showNextQuestion()
        } else {
            alert("« ${question.opt[question.ans]} » ${getString(R.string.text_was_correct)}") {
                positiveButton(getString(R.string.action_next)) { showNextQuestion() }
            }.show()
        }
    }

    private fun getAllQuestions() {
        val type = Types.newParameterizedType(List::class.java, Question::class.java)
        val moshi = Moshi.Builder().build()
        val jsonAdapter: JsonAdapter<List<Question>> = moshi.adapter(type)

        quiz = jsonAdapter.fromJson(loadJSONFromAsset())
    }

    private fun loadJSONFromAsset(): String {
        try {
            val input = activity!!.assets.open(resources.getStringArray(R.array.path_quiz)[activity!!.intent.getIntExtra(App.BUNDLE_PART, 0)])
            val source = Okio.buffer(Okio.source(input))
            return source.readByteString().string(Charset.forName("utf-8"))
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        return ""
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState?.putInt(BUNDLE_QUESTION_INDEX, questionIndex)
        outState?.putInt(BUNDLE_SCORE, score)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null) {
            questionIndex = savedInstanceState.getInt(BUNDLE_QUESTION_INDEX)
            score = savedInstanceState.getInt(BUNDLE_SCORE)
        }
        showQuestion()
    }
}
