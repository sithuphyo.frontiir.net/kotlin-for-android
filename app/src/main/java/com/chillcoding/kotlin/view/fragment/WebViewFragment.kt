package com.chillcoding.kotlin.view.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import kotlinx.android.synthetic.main.fragment_web_view.*
import org.jetbrains.anko.support.v4.browse

/**
 * A simple [Fragment] subclass.
 *
 */
class WebViewFragment() : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_web_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webView.loadUrl(activity!!.intent.getStringExtra(App.BUNDLE_URL))
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
                if (url.contains("file:///android_asset/"))
                    webView.loadUrl(url)
                else
                    browse(url)
                return true
            }

            override fun onLoadResource(view: WebView?, url: String?) {

            }
        }
    }
}
