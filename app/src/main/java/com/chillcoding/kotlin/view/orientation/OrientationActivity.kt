package com.chillcoding.kotlin.view.orientation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.chillcoding.kotlin.App
import com.chillcoding.kotlin.R
import kotlinx.android.synthetic.main.fragment_second_pane.*
import org.jetbrains.anko.browse

/**
 * Created by macha on 16/02/2018.
 */
class OrientationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_orientation)
        orientationGitLabLink.setOnClickListener { browse(App.arrayGitUrl[13]) }
    }
}